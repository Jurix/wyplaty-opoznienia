<?php
Class Contants {
	public $moderators;
	public $delayValues;
	public $gamesWithBonus;
	public $bonusPtsAmmount = 16;
	protected $delayTable = 'opoznienia';
	protected $moderationsTable = 'PrzemoTabela';

	protected function connectDatabase() {
		$mysqli = new mysqli();
		if ($mysqli->connect_errno) {
    		echo "Błąd podczas łączenia z bazą danych. (".$mysqli->connect_errno.") ".$mysqli->connect_error;
    		return false;
		}
		return $mysqli;
	}

	private function getModerators() {
		$this->moderators = [
			['Nick' => 'ThreeG', 'DG' => 52355008, 'GK' => 124158996],
			['Nick' => 'Jurix', 'DG' => 130128, 'GK' => 1537143],
			['Nick' => 'David', 'DG' => 57817, 'GK' => 121096754],
			['Nick' => 'Sekracik', 'DG' => 100225836, 'GK' => 121361518],
			['Nick' => 'Snocky', 'DG' => 80485405, 'GK' => 'none'],
			['Nick' => 'Seqel', 'DG' => 102735674, 'GK' => 121664776],
			['Nick' => 'The.Mat', 'DG' => 102625242, 'GK' => 120006618],
			['Nick' => 'MrShot', 'DG' => 68065607, 'GK' => 119798472],
			['Nick' => 'Teriq', 'DG' => 29780, 'GK' => 119027800],
			['Nick' => 'Nysharrr', 'DG' => 67852308, 'GK' => 107628804],
			['Nick' => 'Semir', 'DG' => 362210, 'GK' => 118109192],
			['Nick' => 'DaQu', 'DG' => 100951656, 'GK' => 112902618],
			['Nick' => 'Natalula', 'DG' => 55828161, 'GK' => 114703220],
			['Nick' => 'Caarel', 'DG' => 4186404, 'GK' => 109424260],
			['Nick' => 'Shoage', 'DG' => 100423830, 'GK' => 107732312],
			['Nick' => 'Kajojcia', 'DG' => 50249228, 'GK' => 107628742],
			['Nick' => 'Nikola', 'DG' => 2910859, 'GK' => 106891696],
			['Nick' => 'FlooraS', 'DG' => 89426120, 'GK' => 100409830],
			['Nick' => 'Radger', 'DG' => 310647, 'GK' => 76813193]
		];
	}

	protected function returnModeratorByID($moderatorID) {
		foreach ($this->moderators as $key => $value) {
			if($value['DG'] == $moderatorID || $value['GK'] == $moderatorID) {
				return $value['Nick'];
			}
		}
		return $moderatorID;
	}

	private function getDelayValue() {
		//data - H, H, pts
		//interval <a, b)
		$this->delayValues = [
			[0, (20/60), 6],
			[(20/60), 4, 5],
			[4, 72, 4],
			[72, INF, 3]
		];
	}

	private function getGamesWithBonus() {
		$this->gamesWithBonus = [];
	}

	public function start() {
		$this->getModerators();
		$this->getDelayValue();
		$this->getGamesWithBonus();
	}
}
?>